source code for [my site](https://aei.sh).

# licensing

layouts, assets, and code for this site fall under Creative Commons Attribution-ShareAlike 4.0 International

* The contents of /img/buttons/ do not fall under CC-BY-SA 4.0

# attributions.
- [cozette](https://github.com/slavfox/Cozette) licensed under [MIT licence](https://github.com/slavfox/Cozette/blob/master/LICENSE).
- [w95fa](https://www.dafont.com/w95fa.font) licensed under SIL OpenFont license.